FROM wyveo/nginx-php-fpm:php74

ADD ./dev/nginx/default.conf /etc/nginx/conf.d/default.conf
ADD html /var/www/html
WORKDIR /var/www/html

