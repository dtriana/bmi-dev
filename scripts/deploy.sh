#!/bin/bash
version=$(<./html/version)
cd dev
echo "Deploying ... $version"
sed -i "s/{version}/$version/" bmi-deployment.yaml
kubectl apply -f bmi-deployment.yaml
