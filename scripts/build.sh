#!/bin/bash
version=$(<./html/version)
pwd
echo "Building.. $version"
docker build -f ./dev/Dockerfile -t dtriana/bmi:$version .
docker push dtriana/bmi:$version