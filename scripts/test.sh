#!/bin/bash
git clone https://bitbucket.org/dtriana/bmi-app.git html
cd html
version=$(<./version)
git checkout $version
composer install
./vendor/bin/phpunit
