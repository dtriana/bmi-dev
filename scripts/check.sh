#!/bin/bash
version=$(<./bmi-app/version)
if GIT_DIR=./bmi-app/.git git rev-parse $version >/dev/null 2>&1
then
    echo "Found tag $version"
else
    echo "Tag $version not found"
    exit 1
fi

