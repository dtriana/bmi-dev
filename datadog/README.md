helm repo add datadog https://helm.datadoghq.com
helm repo add stable https://charts.helm.sh/stable
helm repo update
helm install datadog-monitor -f values.yaml datadog/datadog
